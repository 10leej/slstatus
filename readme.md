# slstatus

Suckless status output, this just adds status text to dwm and it intended to be paired with my dwm build [here](https://gitlab.com/10leej/dwm). It should work with any dwmbuild though since slstatus by nature is pretty much portable.

There are two branches, one for desktop aka "main" and another for laptops
The SSID monitor watches wlan0


The ping timer is set to ping my VPS which runs [my website](https://10leej.com) and we just do typical monitoring of resources we care to track after that.
